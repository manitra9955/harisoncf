<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Documents;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;

class DocumentsController extends Controller {

    /**
     * @Route("/addDocuments/{intitule}/{typeDoc}/{url}") 
     */
    public function addDocumentsAction($intitule, $typeDoc, $url) {
        $d = new Documents();
        $d->setIntitule($intitule);
        $d->setTypeDoc($typeDoc);
        $d->setUrl($url);
        $d->setDate(new \DateTime("now"));
        $em = $this->getDoctrine()->getManager();
        $em->persist($d);
        $em->flush();
        return $this->render('default/adddocuments.html.twig', array('Documents' => $d));
    }

    /**
     * @Route("/listeDocuments",name="liste") 
     */
    public function listeDocumentstAction() {
        $Documents = $this->getDoctrine()->getRepository("AppBundle:Documents")->findAll();
        return $this->render('default/listedocuments.html.twig', array('Documents' => $Documents));
    }

    /**  
     * @Route("/televerser",name="televerser") 
     */
    public function televerserAction(Request $request) {
        $d = new Documents();
//générer le formulaire 
        $form = $this->createFormBuilder($d)
                //  ->add('intitule',FileType::class,array('label'=>'Chosir un fichier'))
                //  ->add('Televerser', SubmitType::class)
                ->getForm();
        $form->handleRequest($request);
//tester si le formuaire est valide
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
//-----
            // $entity->setSize($_FILES['myFile']['size']);
            $dest = 'Documents/'.$_FILES['myFile']['name'];
            $file_tmp_name = $_FILES['myFile']['tmp_name'];
            $d->setIntitule($_FILES['myFile']['name']);
            $d->setTypeDoc(strrchr($_FILES['myFile']['name'],"."));
           // $d->setTypeDoc($_FILES['myFile']['type']);
            $d->setUrl($dest);
            $d->setDate(new \DateTime("now"));
            move_uploaded_file($file_tmp_name, $dest);
            //the attribute Data is the most important one because it's where we put our file Data
//-----
            $em->persist($d);
            $em->flush();
//aller à la vue liste des produits
            return $this->redirect($this->generateUrl("liste"));
        }
        return $this->render('Default/televerser.html.twig', array('f' => $form->createView()));
    }
 /**
     * @Route("/telecharger",name="telecharger") 
     */
    public function telechargerAction(Request $request) {
        $Documents = $this->getDoctrine()->getRepository("AppBundle:Documents")->findAll();
        $httpHost = $request->getBasePath()."/";
        return $this->render('default/telecharger.html.twig', array('Documents' => $Documents,'httpDoc'=>$httpHost));
    }
}
