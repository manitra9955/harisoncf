<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Historique
 *
 * @ORM\Table(name="historique")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\HistoriqueRepository")
 */
class Historique
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nomComplet", type="string", length=255)
     */
    private $nomComplet;

    /**
     * @var string
     *
     * @ORM\Column(name="typeTransac", type="string", length=255, nullable=true)
     */
    private $typeTransac;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateTransac", type="datetimetz", nullable=true)
     */
    private $dateTransac;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nomComplet
     *
     * @param string $nomComplet
     *
     * @return Historique
     */
    public function setNomComplet($nomComplet)
    {
        $this->nomComplet = $nomComplet;

        return $this;
    }

    /**
     * Get nomComplet
     *
     * @return string
     */
    public function getNomComplet()
    {
        return $this->nomComplet;
    }

    /**
     * Set typeTransac
     *
     * @param string $typeTransac
     *
     * @return Historique
     */
    public function setTypeTransac($typeTransac)
    {
        $this->typeTransac = $typeTransac;

        return $this;
    }

    /**
     * Get typeTransac
     *
     * @return string
     */
    public function getTypeTransac()
    {
        return $this->typeTransac;
    }

    /**
     * Set dateTransac
     *
     * @param \DateTime $dateTransac
     *
     * @return Historique
     */
    public function setDateTransac($dateTransac)
    {
        $this->dateTransac = $dateTransac;

        return $this;
    }

    /**
     * Get dateTransac
     *
     * @return \DateTime
     */
    public function getDateTransac()
    {
        return $this->dateTransac;
    }
}

